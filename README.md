# node.h #
#pragma once
class Node
{
public:
	int value;
	Node * prev,
		*next;
	Node();
	~Node();
};
                                  # lista.h #
#pragma once
#include "Node.h"

class DLList
{
private:
	Node *Head,*Tail;
public:
	void insert(const int & v);
	DLList();
    void Delete(const int & _v);
	void Print(const int & v);
	~DLList();
};
                                  # node.cpp #
#pragma once
#include "Node.h"
#include "lista.h"
#include <iostream>

Node::Node() : prev(NULL), next(NULL)
{

}

Node::~Node()
{
}
                                   # lista.cpp #
#include "lista.h"
#include <iostream>

DLList::DLList() : Head(NULL), Tail(NULL)
{}

void DLList::insert(const int & v)
{
	Node * nuevo = new Node();
	nuevo->value = v;
	if (Tail == NULL)
	{ 
		Head = Tail = nuevo;
	}
	else
	{
		nuevo->prev = Tail;
		Tail->next = nuevo;
		Tail = nuevo;
	}
}




	DLList::~DLList()
	{
	}

	void DLList::Delete(const int & v)
	{
		if (Head == nullptr) return;
		Node* ptemp = Head;
		for (int i = 0; i < _v && ptemp != nullptr; ++i)
		{
			ptemp = ptemp->next;
		}
		if (ptemp == nullptr) return;
		if (ptemp->next != nullptr)
			ptemp->next->prev = ptemp->prev;
		if (ptemp->prev != nullptr)
			ptemp->prev->next = ptemp->next;
		delete ptemp;

	}

	void DLList::Print(const int & v)
	{
		Node* pTemp = Head;
		while (pTemp != nullptr)
		{
			std::cout << pTemp->value << std::endl;
			pTemp = pTemp->next;
		}

	}

	void main()
	{
		DLList l;
		l.insert(const int & v);
		l.Delete(const int & v);
		l.Print(const int & v);
	    getchar();
	}